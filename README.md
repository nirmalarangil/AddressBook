# AddressBook
Address book application

This is implemented as a simple java project with a couple of entity classes and a service class.
There is also a junit test class to test the requirement mentioned in the problem statement.
Address book data is persisted in the filesystem as a text file instead of using a database.
All the classes and methods are provided with comments in case anything is not clear.


Assumptions
-------------
There will not be any duplicate contact entry in the address book with the same name.
Two contacts can have the same phone number.
There is no permission issue for the program to write and read data to and from the filesystem.
